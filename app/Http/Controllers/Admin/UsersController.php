<?php

namespace auttana\Http\Controllers\Admin;

use Illuminate\Http\Request;
use auttana\Http\Controllers\Controller;
use auttana\User;
use Laracasts\Flash\Flash;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
       $users = User::orderBy('id','ASC')->paginate(5);
       return view('admin.users.index')->with('users',$users);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {	
    	return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user           = new User($request->all());
        $user->password = bcrypt($request->password);
        $user->save(); 
        flash('Registro de Usuario Exitoso')->success();
        return redirect()->action('Admin\UsersController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $user = User::find($id);
        return view('admin.users.edit')->with('user',$user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $user         = User::find($id);
       $user->name   = $request->name;
       $user->email  = $request->email;
       $user->role   = $request->role;
       $user->save();
       Flash('Actualizaciòn Èxitosa')->success();
       return redirect()->action('Admin\UsersController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        Flash('Usuario Eliminado')->success();
        return redirect()->action('Admin\UsersController@index');
    }
  
}
