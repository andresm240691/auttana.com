<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('inicio');

/*===============================  ADMIN ================================*/
Route::prefix('admin')->group(function (){

	Route::get('home', 'Admin\HomeController@index')->name('adminHome');

	Route::resource('users','Admin\UsersController');

	Route::get('users/{id}/destroy',[
    	'uses' => 'Admin\UsersController@destroy',
    	'as'   => 'admin.users.destroy'
    ]);

    Route::get('users/{id}/edit',[
    	'uses' => 'Admin\UsersController@edit',
    	'as'   => 'admin.users.edit'
    ]);


    // Authentication Routes...
    Route::get('login',   'Admin\LoginController@showLoginForm')->name('login');
    Route::post('login',  'Admin\LoginController@login');
    Route::post('logout', 'Admin\LoginController@logout')->name('logout');


    // Password Reset Routes...
    Route::get('password/reset', 'Admin\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/email', 'Admin\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset/{token}', 'Admin\ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('password/reset', 'Admin\ResetPasswordController@reset');





});


/*
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
*/


// Authentication Routes...
/*
    Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('login', 'Auth\LoginController@login');
    Route::post('logout', 'Auth\LoginController@logout')->name('logout');
*/


/*
    // Registration Routes...
    $this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
    $this->post('register', 'Auth\RegisterController@register');

    // Password Reset Routes...
    $this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    $this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    $this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    $this->post('password/reset', 'Auth\ResetPasswordController@reset');
*/