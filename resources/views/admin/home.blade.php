@extends('layouts.admin')

@section('content')


<div class="card">
  <div class="card-header">
    Inicio
  </div>
  <div class="card-body">
    <h2 class="card-title text-center">Bienvenido</h2>
    <h3 class ="card-title text-center">{{$user->name}}</h3>
    <p class="card-text text-center"> 
        Este m&oacute;dulo corresponde al panel del administraci&oacute;n de Auttana.com<br><br>
        <a href="#" class="btn btn-primary">Comenzar</a>
    </p>
    
  </div>
</div>



@endsection
