@extends('layouts.admin')
@section('content')

<!-- Breadcrumbs-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{action('Admin\UsersController@index')}}">Usuarios</a></li>
    <li class="breadcrumb-item active">Edici&oacute;n</li>
  </ol>
  <!-- Example DataTables Card-->
  <div class="card mb-3">
    <div class="card-header">
       <i class="fa fa-table"></i> Usuario
    </div>
    <div class="card-body">
       {!! Form::open(['action' => ['Admin\UsersController@update',$user], 'method' => 'PUT'])   !!}
			<div class="form-group">
				{!!  Form::label('name','Nombre') !!}
				{!!  Form::text('name',$user->name,['class' => 'form-control','required'])!!}
			</div>

			<div class="form-group">
				{!!  Form::label('email','Corr&eacute;o Electr&oacute;nico') !!}
				{!!  Form::email('email',$user->email,['class' => 'form-control','placeholder' => 'correo@dominio.com','required'])!!}
			</div>

			<div class="form-group">
				{!! Form::label('role','Rol') !!}
				{!! Form::select(
						'role', 
						[
							'Admin'    => 'admin', 
							'Currier'  => 'Currier',
							'Analista' => 'Analista'
						],
						$user->role,
						[
							'class'  => "custom-select"	
						]
					) 
				!!}
			</div>
			<div class="form-group">
				{!! Form::submit('Registrar', ['class' => 'btn btn-primary btn-md'] ) !!}
			</div>
	    {!! Form::close()  !!}
    </div>
    <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
  </div>




@endsection('content')