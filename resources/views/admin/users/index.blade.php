@extends('layouts.admin')
@section('content')
  <!-- Breadcrumbs-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{action('Admin\UsersController@index')}}">Usuarios</a></li>
    <li class="breadcrumb-item active">Listado</li>
  </ol>
  <!-- Example DataTables Card-->
  <div class="card mb-3">
    <div class="card-header">
      <i class="fa fa-table"></i> Usuarios</div>
    <div class="card-body">
      <a href="{{action('Admin\UsersController@create')}}" class ="btn btn-primary btn-md">Nuevo Usuario</a>
      <br><br>
      <div class="table-responsive">

        @include('flash::message')
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>Id</th>
              <th>Nombre</th>
              <th>Corr&eacute;o Electr&oacute;nico</th>
              <th>Rol</th>
              <th>Acci&oacute;n</th>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th>Id</th>
              <th>Nombre</th>
              <th>Corr&eacute;o Electr&oacute;nico</th>
              <th>Rol</th>
              <th>Acci&oacute;n</th>
            </tr>
          </tfoot>
          <tbody>
            @foreach($users as $user)
            <tr>
              <td>{{ $user->id}}</td>
              <td>{{ $user->name}}</td>
              <td>{{ $user->email}}</td>
              <td>{{ $user->role}}</td>
              <td>
                <a href="{{route('admin.users.edit',$user->id)}}" class = "btn btn-success btn-sm">Editar</a>
                <a href="{{route('admin.users.destroy',$user->id)}}" onclick = "return confirm('¿Seguro que desea eliminar el usuario ?');" 
                  class = "btn btn-danger btn-sm">Eliminar</a>
              </td>
            </tr>
            @endforeach
  
          </tbody>
        </table>
        <!-- Paginacion con laravel 
    {!! $users->render()!!}
      -->
      </div>
    </div>
    <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
  </div>
</div>
@endsection('content')