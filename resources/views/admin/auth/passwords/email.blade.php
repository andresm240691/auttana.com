<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Password| Panel de Administraci&oacute;</title>
  <!-- Bootstrap core CSS-->
  <link rel="stylesheet" href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('vendor/font-awesome/css/font-awesome.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/sb-admin.css')}}">

  
</head>

<body class="bg-dark">
  <div class="container">
    <div class="card card-login mx-auto mt-5">
      <div class="card-header text-center">Recuperar Contraseña</div>
      <div class="card-body">
        <!-- *************************************************************************************-->
        <div class="panel-body">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            
            <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                {{ csrf_field() }}
                
                <!-- campo email para recuperar contraseña -->    
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-6 control-label">Corr&eacute;o Electr&oacute;nico</label>
                    <input id          ="email" 
                           type        ="email" 
                           class       ="form-control" 
                           name        ="email" 
                           value       ="{{ old('email') }}" 
                           placeholder ="correo@email.com" 
                           required>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary btn-block">
                                Confirmar
                            </button>
                        </div>
                         <div class="col-md-6 col-md-offset-4">
                            <a href="{{route('login')}}" class="btn btn-secondary btn-block">Atr&aacute;s </a>
                        </div>
                    </div>    
                </div>
            </form>
        </div>
        <!-- *************************************************************************************-->
       
      </div>
    </div>
  </div>
  <!-- Bootstrap core JavaScript-->
  <script src = "{{asset('vendor/jquery/jquery.min.js')}}"></script>
  <script src = "{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src = "{{asset('vendor/jquery-easing/jquery.easing.min.js')}}"></script>

  
</body>

</html>


