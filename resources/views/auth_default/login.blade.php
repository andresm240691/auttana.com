<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Login | Administrador</title>
  <!-- Bootstrap core CSS-->
  <link rel="stylesheet" href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('vendor/font-awesome/css/font-awesome.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/sb-admin.css')}}">

  
</head>

<body class="bg-dark">
  <div class="container">
    <div class="card card-login mx-auto mt-5">
      <div class="card-header text-center">Autana | Administrador</div>
      <div class="card-body">
        <!--
        <form>
          <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input class="form-control" id="exampleInputEmail1" type="email" aria-describedby="emailHelp" placeholder="Enter email">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input class="form-control" id="exampleInputPassword1" type="password" placeholder="Password">
          </div>
          <div class="form-group">
            <div class="form-check">
              <label class="form-check-label">
                <input class="form-check-input" type="checkbox"> Remember Password</label>
            </div>
          </div>
          <a class="btn btn-primary btn-block" href="index.html">Login</a>
        </form>
      -->

        <form class="form-horizontal" method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" >Corr&eacute;o Electr&oacute;nico</label>
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" >Contraseña</label>
                <input id="password" type="password" class="form-control" name="password" required>
                 @if ($errors->has('password'))
                      <span class="help-block">
                          <strong>{{ $errors->first('password') }}</strong>
                      </span>
                 @endif
            </div>
            
            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Recuèrdame
                        </label>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
               
                    <button type="submit" class="btn btn-primary btn-block">
                        Login
                    </button>
                 <div class="col-md-9 col-md-offset-3">  
                    <a class="btn btn-link" href="{{ route('password.request') }}">
                        ¿Olvido su Contraseña?
                    </a>
                </div>
            </div>
        </form>



        <!--
        <div class="text-center">
          <a class="d-block small mt-3" href="register.html">Register an Account</a>
          <a class="d-block small" href="forgot-password.html">Forgot Password?</a>
        </div>
      -->
      </div>
    </div>
  </div>
  <!-- Bootstrap core JavaScript-->
  <script src = "{{asset('vendor/jquery/jquery.min.js')}}"></script>
  <script src = "{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src = "{{asset('vendor/jquery-easing/jquery.easing.min.js')}}"></script>

  
</body>

</html>
